fix :: (a -> a) -> a
fix f = f (fix f)

sum' = fix sum'
  where
    sum' f 0 = 0
    sum' f x = x + f (x-1)

fix' stp = (\x -> stp (x x)) (\x -> stp (x x))
