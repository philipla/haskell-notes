-- If-Then-Else
ite b x y = b x y

true  x y = x
false x y = y

not' b = ite b false true

and' a b = ite a b false

or' a b = ite a true b

main :: IO ()
main = do
  putStrLn (ite true "a" "b")
  putStrLn (ite false "a" "b")

  putStrLn (not' true "true" "false")
  putStrLn (not' false "true" "false")

  putStrLn (and' true true "true" "false")
  putStrLn (and' true false "true" "false")
  putStrLn (and' false true "true" "false")
  putStrLn (and' false false "true" "false")

  putStrLn (or' true true "true" "false")
  putStrLn (or' true false "true" "false")
  putStrLn (or' false true "true" "false")
  putStrLn (or' false false "true" "false")
