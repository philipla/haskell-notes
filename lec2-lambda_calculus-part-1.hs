-- to interact with this file, on the command line type: ghci lec2_lambda_calculus-part-1.hs

-- lambda calculus
--    has only functions
--    define & call a function

-- grammar definition
-- e ::= x
--    | \x -> e
--    | e1 e2

-- grammar contains variables, abstractions (function definition), and application (function call)

-- identity function
-- \x -> x
identity    = \x -> x
identity' x = x

-- function that returns identity function
-- \f -> (\x -> x)
returnIdentity    = (\f -> (\x -> x))
returnIdentity' f = identity

-- function that applies it args to identity function
-- returns what f returns
-- \f -> f (\x -> x)
functionApply    = (\f -> f (\x -> x))
functionApply' f = f identity

-- multiple arguments
add = (\x -> \y -> (x + y))

-- () is a zero element tuple. think of it as a placeholder where when we call
-- a function we are not using it
main :: IO ()
main = do
  putStrLn "Hello, World!"

  putStrLn (identity "Hello, World!")
  putStrLn (identity' "Hello, World!")

  putStrLn (returnIdentity  () "Hello, World!")
  putStrLn (returnIdentity' () "Hello, World!")

  let f = \x -> "Hello, World!"
  putStrLn (functionApply (\x -> "Hello, World!"))
  putStrLn (functionApply' f)

  -- show converts type Integer to type [Char] aka String
  putStrLn (show ((add 1) 2))
  putStrLn (show (add 1 2))
